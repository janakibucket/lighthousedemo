package com.lighthouse.lighthousedemo.controller;

import com.lighthouse.lighthousedemo.entity.Department;
import com.lighthouse.lighthousedemo.entity.Employee;
import com.lighthouse.lighthousedemo.model.DepartmentMO;
import com.lighthouse.lighthousedemo.model.EmployeeMO;
import com.lighthouse.lighthousedemo.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/lighthouse")
public class LighthousedemoController {

    @Autowired
    DepartmentService departmentService;

    @RequestMapping(value = "/prime/{primeText}")
    public boolean getPrime(@PathVariable("primeText") String primeText){
       return primeText.equals("LIGHTHOUSE");
    }

    @GetMapping("/dept/{deptId}")
    @CrossOrigin
    public DepartmentMO getDepartment (@PathVariable int deptId) {

        return departmentService.getDepartment(deptId);
    }
    @GetMapping("/departments")
    @CrossOrigin
    public List<DepartmentMO> getDepartments () {

        return departmentService.getAllDepartment();
    }

    @PostMapping("/SavDept")
    @CrossOrigin
    public ResponseEntity<DepartmentMO> saveDepartment(@RequestBody DepartmentMO dept) {
        Department result = departmentService.saveDepartment(dept);
        if(result ==null) return  ResponseEntity.badRequest().build();
        else{
            DepartmentMO deptMo = new DepartmentMO();
            deptMo.setDeptId(result.getDeptId());
            deptMo.setDeptName(result.getDeptName());
            List<EmployeeMO> empMOList = new ArrayList<>();
            for(Employee emp:result.getEmployees()) {
                EmployeeMO empMo = new EmployeeMO();
                empMo.setEmpId(emp.getEmpId());
                empMo.setEmpName(emp.getEmpName());
                empMOList.add(empMo);
            }
            deptMo.setEmployeeMOS(empMOList);
            return new ResponseEntity<DepartmentMO>(deptMo,HttpStatus.OK);
        }
    }
}
