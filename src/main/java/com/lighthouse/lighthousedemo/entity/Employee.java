package com.lighthouse.lighthousedemo.entity;

import javax.persistence.*;

@Entity
@Table(name = "employee",schema = "lighthouse")
public class Employee {

    @Id
    @Column(name = "emp_id")
    @GeneratedValue( strategy= GenerationType.IDENTITY )
    private int empId;
    @Column(name = "emp_name")
    private String empName;
    @ManyToOne
    @JoinColumn(name = "dept_id")
    private Department department;

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
