package com.lighthouse.lighthousedemo.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="department",schema = "lighthouse")
public class Department {

    @Id
    @Column(name = "dept_id")
    @GeneratedValue( strategy= GenerationType.IDENTITY )
    int deptId;

    @Column(name = "name")
    String deptName;

    @OneToMany(mappedBy = "department",cascade = CascadeType.ALL)
    List<Employee> employees;

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void addEmployees(Employee emp){
        if(emp != null){
            if(employees == null){
                employees = new ArrayList<Employee>();
            }
            emp.setDepartment(this);
            employees.add(emp);
        }


    }
}
