package com.lighthouse.lighthousedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LighthousedemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LighthousedemoApplication.class, args);
	}
}
