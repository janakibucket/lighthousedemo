package com.lighthouse.lighthousedemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeMO {

    private int empId;
    private String empName;
    private DepartmentMO departmentMO;

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public DepartmentMO getDepartmentMO() {
        return departmentMO;
    }

    public void setDepartmentMO(DepartmentMO departmentMO) {
        this.departmentMO = departmentMO;
    }
}
