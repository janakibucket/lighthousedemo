package com.lighthouse.lighthousedemo.model;

import java.util.List;

public class DepartmentMO {

    int deptId;

    String deptName;

    List<EmployeeMO> employeeMOS;

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public List<EmployeeMO> getEmployeeMOS() {
        return employeeMOS;
    }

    public void setEmployeeMOS(List<EmployeeMO> employeeMOS) {
        this.employeeMOS = employeeMOS;
    }


}
