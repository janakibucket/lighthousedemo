package com.lighthouse.lighthousedemo.service.impl;

import com.lighthouse.lighthousedemo.entity.Department;
import com.lighthouse.lighthousedemo.entity.Employee;
import com.lighthouse.lighthousedemo.model.DepartmentMO;
import com.lighthouse.lighthousedemo.model.EmployeeMO;
import com.lighthouse.lighthousedemo.repositorie.DepartmentRepository;
import com.lighthouse.lighthousedemo.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepartmentServiceImpl implements DepartmentService{
    @Autowired
    DepartmentRepository departmentRepository;
    @Override
    public DepartmentMO getDepartment(int deptId) {
        DepartmentMO deptMo = new DepartmentMO();
        try {
            Department dept = null;
            dept =  departmentRepository.findByDeptId(deptId);
            if(dept!= null){
              deptMo.setDeptId(dept.getDeptId());
              deptMo.setDeptName(dept.getDeptName());
                List<EmployeeMO> empMoList = new ArrayList<>();
              for(Employee emp : dept.getEmployees()){
                  EmployeeMO empMo = new EmployeeMO();
                  empMo.setEmpId(emp.getEmpId());
                  empMo.setEmpName(emp.getEmpName());
                  empMoList.add(empMo);
              }
                deptMo.setEmployeeMOS(empMoList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deptMo;
    }

    @Override
    public List<DepartmentMO> getAllDepartment() {
        List<Department> departments;
        List<DepartmentMO> deptMos = new ArrayList<>();
        departments = (List<Department>)departmentRepository.findAll();
        if(departments != null && departments.size() > 0){
            for(Department dept :departments ){
                DepartmentMO deptMo = new DepartmentMO();
                deptMo.setDeptId(dept.getDeptId());
                deptMo.setDeptName(dept.getDeptName());
                List<EmployeeMO> empMos = new ArrayList<>();
                for(Employee emp : dept.getEmployees()){
                    EmployeeMO empMo = new EmployeeMO();
                    empMo.setEmpId(emp.getEmpId());
                    empMo.setEmpName(emp.getEmpName());
                    empMos.add(empMo);
                }
                deptMo.setEmployeeMOS(empMos);
                deptMos.add(deptMo);
            }
        }
        return deptMos;
    }

    @Override
    public Department saveDepartment(DepartmentMO dept) {
        Department result = null;
        Department department = new Department();
        try {
            if (dept != null) {
                department.setDeptId(dept.getDeptId());
                department.setDeptName(dept.getDeptName());
                for(EmployeeMO emp : dept.getEmployeeMOS()){
                    Employee employee = new Employee();
                    employee.setDepartment(department);
                    employee.setEmpId(emp.getEmpId());
                    employee.setEmpName(emp.getEmpName());
                    department.addEmployees(employee);
                }

                result = departmentRepository.save(department);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}

