package com.lighthouse.lighthousedemo.service;

import com.lighthouse.lighthousedemo.entity.Department;
import com.lighthouse.lighthousedemo.model.DepartmentMO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DepartmentService {

    public DepartmentMO getDepartment(int deptId);
    public List<DepartmentMO> getAllDepartment();
    public Department saveDepartment(DepartmentMO dept);

}
