package com.lighthouse.lighthousedemo.repositorie;

import com.lighthouse.lighthousedemo.entity.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee,Integer> {
}
