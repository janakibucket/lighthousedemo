package com.lighthouse.lighthousedemo.repositorie;

import com.lighthouse.lighthousedemo.entity.Department;
import com.lighthouse.lighthousedemo.model.DepartmentMO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department,Integer> {
    Department findByDeptId(int deptId);
}
